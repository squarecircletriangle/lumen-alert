package main

import (
	"fmt"
	"os"

	"github.com/go-resty/resty/v2"
	"github.com/sfreiberg/gotwilio"
)

type SupporterMobile struct {
	Mobile string
}

func DispatchSMSNotifications(alertMessage string, accountID string) {

	var twilioAccountSid = "ACb4e9cfe6f8154ed7ba08515812a213bd"
	var twilioAuthToken = "b269f4886f6156d551f01f882e9b80da"
	var twilio = gotwilio.NewTwilioClient(twilioAccountSid, twilioAuthToken)
	from := os.Getenv("SMS_SENDER_NUMBER")
	message := fmt.Sprintf("Lumin Alert: %s", alertMessage)

	fmt.Println("Sending SMS message for critical alert to all supporters....")

	for _, recipientNumber := range getSMSNumbers(accountID) {
		fmt.Println("\tSending SMS to: ", recipientNumber.Mobile)
		twilio.SendSMS(from, recipientNumber.Mobile, message, "", "")
	}
}

func getSMSNumbers(accountID string) []SupporterMobile {
	fmt.Println("getSMSNumbers...")
	var supporterMobileNumbers []SupporterMobile

	client := resty.New()
	resp, err := client.R().
		SetHeader("Accept", "application/json").
		SetResult(&supporterMobileNumbers).
		SetAuthToken(GenerateToken(accountID)).
		Get("https://api.mylumin.net/api/v1/carers")

	if err != nil {
		fmt.Println("Error recovering carer details from Lumen API: ", err.Error())
		return supporterMobileNumbers
	}
	fmt.Println("Retrieved data: ", resp.Result().(*[]SupporterMobile))

	// supporterMobileNumbers = resp.Result().(*[]SupporterMobile)

	fmt.Println(supporterMobileNumbers)
	// if err := json.Unmarshal(resp.Body(), &supporterMobileNumbers); err != nil {
	// 	fmt.Println(err)
	// }

	return supporterMobileNumbers
}
