package main

import (
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx/types"
	"gopkg.in/guregu/null.v3"
	"time"
)

type IncidentEvent struct {
	ID           int              `db:"id" json:"id"`
	UUID         null.String      `db:"uuid" json:"uuid"`
	incidentUUID null.String      `db:"incident_uuid" json:"incident_uuid"`
	Incident     Incident         // ??
	EventType    IncidentStatus   `db:"event_type" json:"event_type"`
	Severity     IncidentSeverity `db:"severity" json:"severity"`
	Label        null.String      `db:"label" json:"label"`
	Data         types.JSONText   `db:"data" json:"data"`
	CreatedBy    int              `db:"created_by" json:"created_by"`
	CreatedAt    time.Time        `db:"created_at" json:"created_at"`
	UpdatedAt    time.Time        `db:"updated_at" json:"updated_at"`
}

func (incidentEvent IncidentEvent) Create() (IncidentEvent, bool) {
	fmt.Println("\tCreating IncidentEvent.... %s", incidentEvent)

	arg := map[string]interface{}{
		"incident_uuid": incidentEvent.incidentUUID,
		"event_type":    incidentEvent.EventType,
		"severity":      incidentEvent.Severity,
		"label":         incidentEvent.Label,
		//"data":        incidentEvent.Data,
		"created_by": incidentEvent.CreatedBy,
		"now":        time.Now(),
	}

	_, err = db.NamedExec(`INSERT INTO incident_events `+
		`(uuid, incident_uuid, event_type, severity, label, created_by, created_at, updated_at) `+
		`VALUES (uuid_generate_v4(), :incident_uuid, :event_type, :severity, :label, :created_by, :now, :now)`, arg)

	if err != nil {
		fmt.Println("ERR: %s", err)
		return incidentEvent, false
	}

	fmt.Println("\tSaved IncidentEvent with type: ", incidentEvent.EventType.String())

	DispatchNotification(incidentEvent)

	return incidentEvent, true
}

func GetIncidentEventsForIncident(incidentUUID string) (IncidentEvent, bool) {
	incidentEvent := IncidentEvent{}
	foundRecord := false
	err = db.Get(&incidentEvent, "SELECT * FROM incident_events WHERE incident_uuid=$1", incidentUUID)
	if err == sql.ErrNoRows {
		fmt.Println("\tNo IncidentEvent Found!")
	}
	if err == nil || err != sql.ErrNoRows {
		fmt.Println("\tFound incidentEvent: %s", incidentEvent)
		foundRecord = true
	}
	return incidentEvent, foundRecord
}
