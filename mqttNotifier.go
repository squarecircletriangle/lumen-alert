package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type MQTTIncidentAlert struct {
	To        string            `json:"to"`
	AlertData IncidentAlertData `json:"payload"`
}

type V2MQTTIncidentAlert struct {
	PackageName   string            `json:"packageName"`   //"com.blocksglobal.lumen.emergency",
	PublishedDate int64             `json:"publishedDate"` //time.Now().Unix() 1684863649000,
	Notification  *string           `json:"notification"`  // null. Not used.
	AlertData     IncidentAlertData `json:"data"`
}

func DispatchMqttNotification(alertData IncidentAlertData, accountID string, accountUUID string) {

	mqtt.ERROR = log.New(os.Stdout, "", 0)
	opts := mqtt.NewClientOptions().AddBroker(os.Getenv("MQTT_HOST")).SetClientID("lumin-alerts")

	opts.SetUsername(os.Getenv("MQTT_USERNAME"))
	opts.SetPassword(os.Getenv("MQTT_PASSWORD"))

	c := mqtt.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	topic := "notifications/" + accountUUID

	if accountUUID == "" {
		topic = "notifications/" + accountID
	}
	var payload []byte

	iAlert := V2MQTTIncidentAlert{PackageName: "com.blocksglobal.lumen.emergency", PublishedDate: time.Now().Unix(), AlertData: alertData, Notification: nil}
	payload, _ = json.Marshal(iAlert)
	fmt.Println("SENDING V2 Payload: ", string(payload))

	fmt.Println("Publishing message to: ", topic)
	token := c.Publish(topic, 0, false, payload)
	token.Wait()
	c.Disconnect(250)
}
