package main

import (
	"fmt"

	"github.com/go-resty/resty/v2"
)

type IncidentGCMAlert struct {
	Title           string           `json:"title"`
	Body            string           `json:"body"`
	AlertUUID       string           `json:"alert_uuid"`
	AlertIdentifier string           `json:"alert_identifier"`
	Type            IncidentStatus   `json:"type"`
	Severity        IncidentSeverity `json:"severity"`
	Label           string           `json:"label"`
	CreatedBy       int              `json:"created_by"`
}

type PnPushTarget struct {
	Environment string `json:"environment"`
	Topic       string `json:"topic"`
}

type PnPush struct {
	PushType string         `json:"push_type"`
	Targets  []PnPushTarget `json:"targets"`
	Version  string         `json:"version"`
}

type IncidentAPNAlert struct {
	Alert string `json:"alert"`
	Badge int    `json:"badge"`
	Sound string `json:"sound"`
}
type IncidentAPSAlert struct {
	APS             IncidentAPNAlert `json:"aps"`
	PNPush          []PnPush         `json:"pn_push"`
	AlertUUID       string           `json:"alert_uuid"`
	AlertIdentifier string           `json:"alert_identifier"`
	Type            IncidentStatus   `json:"type"`
	Severity        IncidentSeverity `json:"severity"`
	Label           string           `json:"label"`
	CreatedBy       int              `json:"created_by"`
}
type IncidentGCMNotificationAlert struct {
	Data IncidentGCMAlert `json:"data"`
}

type IncidentAlert struct {
	APNSAlert IncidentAPSAlert             `json:"pn_apns"`
	GCMAlert  IncidentGCMNotificationAlert `json:"pn_gcm"`
	AlertData IncidentAlertData            `json:"data"`
}

func DispatchPubnubNotification(alertData IncidentAlertData, accountID string, alertMessage string) {

	pnPushTarget := PnPushTarget{Environment: "production", Topic: "com.blocksglobal.lumen.LumenCare"}
	pnPush := PnPush{PushType: "alert", Targets: []PnPushTarget{pnPushTarget}, Version: "v2"}

	apnAlert := IncidentAPSAlert{
		PNPush: []PnPush{pnPush},
		APS: IncidentAPNAlert{
			Alert: alertMessage,
			Badge: NumberOfActiveIncidents(accountID),
			Sound: "bingbong.aiff"},
		AlertUUID:       alertData.AlertUUID,
		AlertIdentifier: alertData.AlertIdentifier,
		Type:            alertData.Type,
		Severity:        alertData.Severity,
		Label:           alertData.Label,
		CreatedBy:       alertData.CreatedBy}

	gcmAlert := IncidentGCMNotificationAlert{
		Data: IncidentGCMAlert{
			Title:           "Lumin Alert",
			Body:            alertMessage,
			AlertUUID:       alertData.AlertUUID,
			AlertIdentifier: alertData.AlertIdentifier,
			Type:            alertData.Type,
			Severity:        alertData.Severity,
			Label:           alertData.Label,
			CreatedBy:       alertData.CreatedBy}}

	iAlert := IncidentAlert{apnAlert, gcmAlert, alertData}

	ch := "lumen:alerts:" + accountID
	fmt.Println("PUBLISHING TO : " + ch)
	sendMessage(ch, iAlert)

	newCh := "accounts." + accountID + ".alerts"
	fmt.Println("PUBLISHING TO : " + newCh)
	sendMessage(newCh, iAlert)

}

func sendMessage(channel string, iAlert IncidentAlert) {

	var subKey = "sub-c-e4adce1e-6f9d-11ea-bbe3-3ec3e5ef3302"
	var pubKey = "pub-c-86fc38d1-7d4d-4bb0-b7ca-d7ca753efe45"
	var uuid = "68b3f66b-d5b9-47dc-b73d-7a8898ac443b"

	var url = "https://ps.pndsn.com/publish/" + pubKey + "/" + subKey + "/0/" + channel + "/0?uuid=" + uuid

	client := resty.New()
	res, err := client.R().
		SetBody(iAlert).
		Post(url)

	if err != nil {
		fmt.Println("Error recovering carer details from Lumen API: %s", err.Error())
	}

	// handle publish result
	fmt.Println("\tPublished to PubNub: ", res, err)
}
