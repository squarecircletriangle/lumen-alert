package main

import (
	"fmt"
)

type IncidentAlertData struct {
	AlertUUID       string           `json:"alert_uuid"`
	AlertIdentifier string           `json:"alert_identifier"`
	Type            IncidentStatus   `json:"type"`
	Severity        IncidentSeverity `json:"severity"`
	Label           string           `json:"label"`
	CreatedBy       int              `json:"created_by"`
}

func DispatchNotification(incidentEvent IncidentEvent) {
	fmt.Println("Sending Alert for: ", incidentEvent.Incident.Identifier.String)

	alertMessage := incidentEvent.Label.String
	accountID := incidentEvent.Incident.AccountID

	if incidentEvent.EventType != triggered {
		alertMessage = fmt.Sprintf("Alert '%s' has been %s", incidentEvent.Label.String, incidentEvent.EventType.String())
	}

	alertData := IncidentAlertData{
		AlertUUID:       incidentEvent.incidentUUID.String,
		AlertIdentifier: incidentEvent.Incident.Identifier.String,
		Type:            incidentEvent.EventType,
		Severity:        incidentEvent.Severity,
		Label:           incidentEvent.Label.String,
		CreatedBy:       incidentEvent.CreatedBy}

	if incidentEvent.Severity == critical {
		DispatchSMSNotifications(alertMessage, accountID)
	}

	DispatchPubnubNotification(alertData, accountID, alertMessage)

	DispatchMqttNotification(alertData, accountID, incidentEvent.Incident.AccountUUID)
}
