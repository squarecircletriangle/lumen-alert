package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

// IncidentResolver is used purely to return a boolean success flag in json response.
type IncidentResolver struct {
	Success bool `json:"success"`
}

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome!\n")
}

func StatusHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "OK\n")
}

////////////////////////////////////////////////////////////////////

func IncidentsIndexHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	incidents := Incidents{}
	accountID, _, _ := extractJWTCredentials(r)
	err = db.Select(&incidents, "SELECT * FROM incidents where account_id = $1 AND status <> $2 ORDER BY id ASC", accountID, resolved)
	if err != nil {
		panic(err)
	}
	if err := json.NewEncoder(w).Encode(incidents); err != nil {
		panic(err)
	}
}

func IncidentUpdateHandler(rw http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(rw, "incident update")
}

func IncidentDeleteHandler(rw http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(rw, "incident delete")
}

func IncidentShowHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var incidentID int
	var err error
	if incidentID, err = strconv.Atoi(vars["id"]); err != nil {
		panic(err)
	}

	incident := GetIncidentByID(incidentID)

	if incident.ID > 0 {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(incident); err != nil {
			panic(err)
		}
		return
	}

	// If we didn't find it, 404
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusNotFound)
	if err := json.NewEncoder(w).Encode(jsonErr{Code: http.StatusNotFound, Text: "Not Found"}); err != nil {
		panic(err)
	}

}

func IncidentResolveHandler(w http.ResponseWriter, r *http.Request) {
	//identifier := r.PostFormValue("identifier")

	var params map[string]string
	body, err := io.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	if err := json.Unmarshal(body, &params); err != nil {
		fmt.Println(err)
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
		return
	}

	fmt.Println("params: ", params)
	//description := vars["description"]
	accountID, _, carerID := extractJWTCredentials(r)

	result := ResolveIncident(params["identifier"], accountID, carerID)

	if err := json.NewEncoder(w).Encode(IncidentResolver{result}); err != nil {
		panic(err)
	}
}

func IncidentAcknowledgeHandler(w http.ResponseWriter, r *http.Request) {
	var params map[string]string
	body, err := io.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	if err := json.Unmarshal(body, &params); err != nil {
		fmt.Println(err)
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
		return
	}

	fmt.Println("params: ", params)
	//description := vars["description"]
	accountID, _, carerID := extractJWTCredentials(r)

	result := AcknowledgeIncident(params["identifier"], accountID, carerID)

	// ---- Response -------------------- //
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err := json.NewEncoder(w).Encode(IncidentResolver{result}); err != nil {
		panic(err)
	}
}

/*
IncidentCreateHandler is responsible for creating new incidents.
You can test with this curl command:
curl -H "Content-Type: application/json" -d '{"fullname":"Nick Marfleet", "email":"nick@whatevernext.org", "password":"mypass", "password_confirmation":"mypass", "nickname":"nick"}' http://localhost:8080/incidents
*/
func IncidentCreateHandler(w http.ResponseWriter, r *http.Request) {
	accountID, accountUUID, _ := extractJWTCredentials(r)

	var incident Incident
	body, err := io.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	if err := json.Unmarshal(body, &incident); err != nil {
		fmt.Println(err)
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
		return
	}

	_, err = govalidator.ValidateStruct(incident)
	if err != nil {
		println("error: " + err.Error())
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
		return
	}

	incident.AccountID = accountID
	incident.AccountUUID = accountUUID
	var success bool
	incident, success = incident.Create()

	if success {
		w.WriteHeader(http.StatusCreated)
	} else {
		w.WriteHeader(409)
	}
	if err := json.NewEncoder(w).Encode(incident); err != nil {
		panic(err)
	}
}

func extractJWTCredentials(r *http.Request) (string, string, int) {
	return ExtractClaims(r.Context().Value("jwt"))

}
