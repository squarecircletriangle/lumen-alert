FROM golang:1.21.0

# cache buster
ENV SYSTEM_UPDATE=17
RUN mkdir /lumen-alert
WORKDIR /lumen-alert

COPY cacert-2017-06-07.pem /etc/ssl/certs/cacert-2017-06-07.pem

COPY . .
ARG GOOS=linux
ARG GOARCH=amd64
# RUN go install bitbucket.org/squarecircletriangle/lumen-alert@master
RUN go get -v -t .   
RUN go build
RUN go install 

EXPOSE 3000

CMD lumen-alert
