package main

import (
	//	"encoding/base64"
	"fmt"
	"log"
	"os"

	"github.com/auth0/go-jwt-middleware"
	"github.com/codegangsta/negroni"
	"github.com/form3tech-oss/jwt-go"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	"github.com/rs/cors"
)

var db *sqlx.DB
var err error

// var routes = Routes{
// 	Route{"Index", "GET", "/", IndexHandler},
// 	Route{"Status", "GET", "/status", StatusHandler},
// }

// var apiRoutes = Routes{
// 	Route{"IncidentIndex", "GET", "/incidents", IncidentsIndexHandler},
// 	Route{"IncidentCreate", "POST", "/incidents", IncidentCreateHandler},
// 	Route{"IncidentShow", "GET", "/incidents/{id}", IncidentShowHandler},
// }

func main() {
	godotenv.Load()

	StartServer()
}

func StartServer() {
	initDB()

	jwtMiddleware := jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return []byte(os.Getenv("JWT_SECRET")), nil
		},
		UserProperty: "jwt",
	})

	cors := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "PUT"},
		AllowedHeaders: []string{"Content-Type", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Accept", "Authorization"},
		Debug:          true,
	})

	r := mux.NewRouter()
	apiRouter := mux.NewRouter().PathPrefix("/v1").Subrouter()

	r.HandleFunc("/status", StatusHandler)
	r.HandleFunc("/", IndexHandler)

	api := negroni.New(
		negroni.NewRecovery(),
		negroni.Handler(cors),
		negroni.HandlerFunc(jwtMiddleware.HandlerWithNext),
		negroni.Wrap(apiRouter),
	)

	// Incidents collection
	incidents := apiRouter.Path("/incidents").Subrouter()
	incidents.Methods("POST").HandlerFunc(IncidentCreateHandler)
	incidents.Methods("GET").HandlerFunc(IncidentsIndexHandler)

	resolveIncident := apiRouter.Path("/incidents/resolve").Subrouter()
	resolveIncident.Methods("POST").HandlerFunc(IncidentResolveHandler)

	ackIncident := apiRouter.Path("/incidents/acknowledge").Subrouter()
	ackIncident.Methods("POST").HandlerFunc(IncidentAcknowledgeHandler)

	r.PathPrefix("/v1").Handler(api)

	server := negroni.Classic()
	server.UseHandler(r)
	server.Run(":3000")
}

func initDB() {
	connectionString := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable", os.Getenv("DB_HOST"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_NAME"))
	fmt.Println("connecting to DB: " + connectionString)
	db, err = sqlx.Connect("postgres", connectionString)
	if err != nil {
		log.Fatalln(err)
	}
}
