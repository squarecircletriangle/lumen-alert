package main

import (
	"database/sql"
	"database/sql/driver"
	"fmt"
	"github.com/jmoiron/sqlx/types"
	"gopkg.in/guregu/null.v3"
	"time"
)

type IncidentCategory int64

const (
	biometrics IncidentCategory = iota
	envirometrics
	system
	userAction
	unacknowledgedReminder
)

func (ic *IncidentCategory) Scan(value interface{}) error {
	*ic = IncidentCategory(value.(int64))
	return nil
}
func (ic IncidentCategory) Value() (driver.Value, error) { return int64(IncidentCategory(ic)), nil }

type IncidentSeverity int64

const (
	ok IncidentSeverity = iota
	warn
	critical
)

func (u *IncidentSeverity) Scan(value interface{}) error {
	*u = IncidentSeverity(value.(int64))
	return nil
}
func (u IncidentSeverity) Value() (driver.Value, error) { return int64(IncidentSeverity(u)), nil }

type IncidentStatus int64

const (
	triggered IncidentStatus = iota
	acknowledged
	resolved
	assigned
)

func (u *IncidentStatus) Scan(value interface{}) error { *u = IncidentStatus(value.(int64)); return nil }
func (u IncidentStatus) Value() (driver.Value, error)  { return int64(IncidentStatus(u)), nil }

type Incident struct {
	ID               int              `db:"id" json:"id"`
	UUID             null.String      `db:"uuid" json:"uuid"`
	AccountID      	 string           `db:"account_id" json:"account_id"`
	AccountUUID      string           `db:"account_uuid" json:"account_uuid"`
	SourceDeviceUUID null.String      `db:"source_device_uuid" json:"source_device_uuid"`
	SourceDeviceName null.String      `db:"source_device_name" json:"source_device_name"`
	Identifier       null.String      `db:"identifier" json:"identifier"`
	AssignedTo       int              `db:"assigned_to" json:"assigned_to"`
	Slug             string  	      `db:"slug" json:"slug" valid:"ascii,required"`
	Category         IncidentCategory `db:"category" json:"category"`
	Severity         IncidentSeverity `db:"severity" json:"severity"`
	Status           IncidentStatus   `db:"status" json:"status"`
	Label            null.String      `db:"label" json:"label"`
	Data             types.JSONText   `db:"data" json:"data"`
	CreatedAt        time.Time        `db:"created_at" json:"created_at"`
	UpdatedAt        time.Time        `db:"updated_at" json:"updated_at"`
}

type Incidents []Incident

func (incident Incident) Create() (Incident, bool) {
	fmt.Println("Creating Incident: %s", incident)
	ns := null.NewString(":", true)
	incident.Identifier = null.NewString(incident.SourceDeviceUUID.String+ns.String+incident.Category.String()+ns.String+incident.Slug, true)

	_, foundRecord := GetActiveIncidentByAccountAndIdentifier(incident.AccountID, incident.Identifier.String)
	if foundRecord {
		fmt.Println("Refusing to create duplicate incident.")
		return incident, false
	}

	arg := map[string]interface{}{
		"account_id":         incident.AccountID,
		"account_uuid":       incident.AccountUUID,
		"source_device_uuid": incident.SourceDeviceUUID,
		"source_device_name": incident.SourceDeviceName,
		"identifier":         incident.Identifier,
		"assigned_to":        incident.AssignedTo,
		"slug":               incident.Slug,
		"category":           incident.Category,
		"severity":           incident.Severity,
		"status":             0,
		"label":              incident.Label,
		"data":               incident.Data,
		"now":                time.Now(),
	}

	stmt, err := db.PrepareNamed(`INSERT INTO incidents
	(uuid, account_id, account_uuid, source_device_uuid, source_device_name, identifier, assigned_to, slug,
	category, severity, status, label, data, created_at, updated_at) VALUES (` +
		`uuid_generate_v4(), :account_id, :account_uuid, :source_device_uuid, :source_device_name, :identifier,
	:assigned_to, :slug, :category, :severity, :status, :label, :data, :now, :now) RETURNING uuid`)

	if err != nil {
		fmt.Println("ERR: %s", err)
		return incident, false
	}

	err = stmt.Get(&incident.UUID, arg)

	if err != nil {
		fmt.Println("ERR: %s", err)
		return incident, false
	}

	_, returnSuccess := createIncidentEvent(incident, triggered, 0)

	return incident, returnSuccess
}

func ResolveIncident(incidentIdentifier string, accountID string, carerID int) bool {
	returnSuccess := false

	incident, foundRecord := GetActiveIncidentByAccountAndIdentifier(accountID, incidentIdentifier)
	if !foundRecord {
		fmt.Println("Unable to find an active incident matching this identifier!")
		return returnSuccess
	}

	// Both triggered and acknowledged incidents may transition to resolved.
	_, err := db.Exec("UPDATE incidents SET (status, updated_at) = ($1, $2) WHERE identifier=$3 AND status <> $1",
		resolved, time.Now(), incidentIdentifier)

	if err == nil {
		_, returnSuccess = createIncidentEvent(incident, resolved, carerID)
	}
	return returnSuccess
}

func AcknowledgeIncident(incidentIdentifier string, accountID string, carerID int) bool {
	returnSuccess := false

	incident, foundRecord := GetActiveIncidentByAccountAndIdentifier(accountID, incidentIdentifier)
	if !foundRecord {
		fmt.Println("Unable to find an active incident matching this identifier!")
		return returnSuccess
	}

	// Only triggered incidents should transition to acknowledged
	_, err := db.Exec("UPDATE incidents SET (status, updated_at) = ($1, $2) WHERE identifier=$3 AND status=$4",
		acknowledged, time.Now(), incidentIdentifier, triggered)

	if err == nil {
		_, returnSuccess = createIncidentEvent(incident, acknowledged, carerID)
	}
	return returnSuccess
}

func GetActiveIncidentByAccountAndIdentifier(accountID string, incidentIdentifier string) (Incident, bool) {
	// It shouldn't be necessary to include accountID in this check, because the device_source_uuid
	// from the identifier will ensure the identifier is ALWAYS globally unique

	incident := Incident{}
	foundRecord := false
	err = db.Get(&incident, "SELECT * FROM incidents WHERE account_id=$1 AND identifier=$2 AND status <> $3", accountID, incidentIdentifier, resolved)
	if err == sql.ErrNoRows {
		fmt.Println("\tNo Incident Found!")
	}
	if err == nil || err != sql.ErrNoRows {
		fmt.Println("\tFound existing Incident: %s", incident)
		foundRecord = true
	}
	return incident, foundRecord
}

func createIncidentEvent(incident Incident, status IncidentStatus, carerID int) (IncidentEvent, bool) {
	incidentEvent := IncidentEvent{
		Incident:     incident,
		incidentUUID: incident.UUID,
		EventType:    status,
		Severity:     incident.Severity,
		Label:        incident.Label,
		CreatedBy:    carerID}

	return incidentEvent.Create()
}

func GetIncidentByID(incidentID int) Incident {
	incident := Incident{}
	err = db.Get(&incident, "SELECT * FROM incidents WHERE id=$1", incidentID)
	return incident
}

// TODO create an Account package to handle scoped incident data calls
func NumberOfActiveIncidents(accountID string) int {
	var id int
	err = db.Get(&id, "SELECT count(*) FROM incidents WHERE account_id=$1 AND status <> $2", accountID, resolved)
	return id
}
