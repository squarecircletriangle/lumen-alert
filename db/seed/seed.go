package main
//
//import (
//	"github.com/jmoiron/sqlx/types"
//	"fmt"
//	"github.com/jmoiron/sqlx"
//	"github.com/joho/godotenv"
//	_ "github.com/lib/pq"
//	"gopkg.in/guregu/null.v2"
//	"os"
//	"github.com/twinj/uuid"
//	"time"
//)
//
//type Incident struct {
//	Id               int              `db:"id" json:"id"`
//	Uuid             uuid.UUID        `db:"uuid" json:"uuid"`
//	AccountUuid      uuid.UUID        `db:"account_uuid" json:"account_uuid"`
//	SourceDeviceUuid uuid.UUID        `db:"source_device_uuid" json:"source_device_uuid"`
//	SourceDeviceName null.String      `db:"source_device_name" json:"source_device_name"`
//	Identifier       null.String      `db:"identifier" json:"identifier"`
//	AssignedTo       int              `db:"assigned_to" json:"assigned_to"`
//	Slug             null.String      `db:"slug" json:"slug" valid:"alphanum,required"`
//	Category         IncidentCategory `db:"category" json:"category"`
//	Severity         IncidentSeverity `db:"severity" json:"severity"`
//	Status           IncidentStatus   `db:"status" json:"status"`
//	Label            null.String      `db:"label" json:"label"`
//	Data             types.JSONText   `db:"data" json:"data"`
//	CreatedAt        time.Time        `db:"created_at" json:"created_at"`
//	UpdatedAt        time.Time        `db:"updated_at" json:"updated_at"`
//}
//
//var db *sqlx.DB
//var err error
//
//func main() {
//	err := godotenv.Load()
//	if err != nil {
//		fmt.Println("Error loading .env file")
//	}
//	initDB()
//
//	//rows, _ := db.Query("delete * from incidents")
//
//	// Create Incidents
//	"INSERT INTO incidents (uuid, account_uuid, source_device_uuid, source_device_name, identifier, assigned_to, slug, category, severity, status, label, data, created_at, updated_at) VALUES (uuid_generate_v4(), uuid_generate_v4(), uuid_generate_v4(), 'Main Tablet', 'Main Tablet/system/battery-low', 123, 'battery-low', 2, 1, 0, 'Your device has low battery', null, 'Wed 4 Nov 2015 11:37:59', 'Wed 4 Nov 2015 11:37:59');"
//}
//
//func initDB() {
//	connectionString := fmt.Sprintf("incident=%s dbname=%s sslmode=disable", os.Getenv("DB_USER"), os.Getenv("DB_NAME"))
//	fmt.Println(connectionString)
//	db, err = sqlx.Connect("postgres", connectionString)
//	if err != nil {
//		fmt.Println(err)
//	}
//}
//
//func addIncidentToDB(incident Incident) {
//	fmt.Println("Inserting: %s", incident.Fullname)
//	_, err = db.NamedExec("INSERT INTO incidents (fullname, nickname, email, password, password_confirmation, phone_number) VALUES (:fullname, :nickname, :email, :password, :password_confirmation, :phone_number)", &incident)
//}
