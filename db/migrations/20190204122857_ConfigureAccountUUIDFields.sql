
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE incidents RENAME COLUMN account_uuid TO account_id;
ALTER TABLE incidents  ADD COLUMN account_uuid VARCHAR;
-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE incidents  DROP COLUMN account_uuid;
ALTER TABLE incidents RENAME COLUMN account_id TO account_uuid;
