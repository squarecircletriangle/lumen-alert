
-- +goose Up
CREATE TABLE incidents(
  id  					SERIAL PRIMARY KEY,
  uuid		 			varchar(150), -- UUID NOT NULL, <<< IMPLEMENTING WITH NATIVE UUID TYPE IS PROBLEMATIC IN GO RIGHT NOW
  account_uuid		 	varchar(150), -- UUID,
  source_device_uuid 	varchar(150), -- UUID,
  source_device_name	varchar(255),
  assigned_to			int,
  identifier			varchar(255),
  slug					varchar(255),
  category				int,
  severity				int,
  status				int,
  label					varchar(255),	
  data					json,
  created_at			timestamp,
  updated_at			timestamp
);

-- +goose Down
drop table incidents;
