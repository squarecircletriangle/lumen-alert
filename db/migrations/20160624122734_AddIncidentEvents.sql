
-- +goose Up
CREATE TABLE incident_events(
  id  			SERIAL PRIMARY KEY,
  uuid		 	varchar(150), -- UUID NOT NULL, <<< IMPLEMENTING WITH NATIVE UUID TYPE IS PROBLEMATIC IN GO RIGHT NOW
  incident_uuid varchar(150),
  event_type    int,
  severity		int,
  label			varchar(255),
  data			json,
  created_by    int,
  created_at	timestamp,
  updated_at	timestamp
);

-- +goose Down
drop table incident_events;
