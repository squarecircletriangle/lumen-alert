package main

import (
	"github.com/form3tech-oss/jwt-go"
	"fmt"
	"os"
	"strconv"
)


func ExtractClaims(tokenData interface{}) (string, string, int) {
	var carerID int
	var accountUUID string
	var foundaUUID bool

	token := tokenData.(*jwt.Token)
	aID := int(token.Claims.(jwt.MapClaims)["account_id"].(float64))
	accountUUID, foundaUUID = token.Claims.(jwt.MapClaims)["sub"].(string)

	if !foundaUUID {
		accountUUID = ""
	}

	cID := token.Claims.(jwt.MapClaims)["carer_id"]
	if cID != nil {
		carerID = int(cID.(float64))
	} else {
		carerID = 0
	}
	accountID := strconv.Itoa(aID)
	fmt.Println("JWT Credentials: [AccountUUID: " + accountUUID + "] [AccountID: " + accountID + "] [carerID: " + strconv.Itoa(carerID) + "]")
	return accountID, accountUUID, carerID
}


func GenerateToken(accountID string) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"account_id": accountID,
	})

	tokenString, _ := token.SignedString([]byte(os.Getenv("JWT_SECRET")))

	return tokenString
}
