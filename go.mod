module bitbucket.org/squarecircletriangle/lumen-alert

go 1.21.0

require (
	github.com/asaskevich/govalidator v0.0.0-20230301143203-a9d515a09cc2
	github.com/auth0/go-jwt-middleware v1.0.1
	github.com/codegangsta/negroni v1.0.0
	github.com/eclipse/paho.mqtt.golang v1.4.3
	github.com/form3tech-oss/jwt-go v3.2.5+incompatible
	github.com/go-resty/resty/v2 v2.7.0
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.9
	github.com/rs/cors v1.9.0
	github.com/sfreiberg/gotwilio v1.0.0
	gopkg.in/guregu/null.v3 v3.5.0
)

require (
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/gorilla/schema v1.1.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
)
